;var data = {
	"aem-fed": {
		"name": "aem-fed",
		"version": "8.0.9",
		"consumers": {}
	},
	"auspost-gulp-es6": {
		"name": "auspost-gulp-es6",
		"version": "1.0.0",
		"consumers": {}
	},
	"auspost-styles": {
		"name": "auspost-styles",
		"version": "1.6.8",
		"consumers": {
			"aem-fed": "^1.0.1",
			"auspost-gulp-es6": "^1.6.1",
			"auspost-ev-ui-demo": "^1.6.1",
			"identity-registration-ui": "^1.6.5",
			"my-post-consumer-track-ui": "^1.4.4",
			"my-post-redirect-ui": "^1.6.2",
			"customer-account-management-ui": "^1.6.1",
			"auspost-address-completion": "^1.3.0"
		}
	},
	"auspost-global-styles": {
		"name": "auspost-global-styles",
		"version": "1.0.0",
		"consumers": {}
	},
	"auspost-ev-ui-demo": {
		"name": "auspost-ev-ui-demo",
		"version": "1.0.0",
		"consumers": {}
	},
	"evapp": {
		"name": "evapp",
		"version": "0.0.0",
		"consumers": {}
	},
	"evtestui": {
		"name": "evtestui",
		"version": "1.0.0",
		"consumers": {}
	},
	"aem-component-grunt": {
		"name": "aem-component-grunt",
		"version": "0.0.1",
		"consumers": {}
	},
	"auspost-identity-ui": {
		"name": "auspost-identity-ui",
		"version": "0.0.1",
		"consumers": {}
	},
	"authority-to-leave-ui": {
		"name": "authority-to-leave-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"cssoapi-swagger-express-mock": {
		"name": "cssoapi-swagger-express-mock",
		"version": "1.0.0",
		"consumers": {}
	},
	"customer-account-management-ui": {
		"name": "customer-account-management-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"driver-dashboard-ui-hack": {
		"name": "driver-dashboard-ui-hack",
		"version": "0.0.1",
		"consumers": {}
	},
	"identity-registration-ui": {
		"name": "identity-registration-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"marketing": {
		"name": "marketing",
		"version": "1.0.0",
		"consumers": {}
	},
	"mentor-ui": {
		"name": "mentor-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-deliveries-ui-integration-tests": {
		"name": "my-deliveries-ui-integration-tests",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-accounts-ui-deprecated": {
		"name": "my-post-accounts-ui-deprecated",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-authenticate-ui": {
		"name": "my-post-authenticate-ui",
		"version": "1.0.1",
		"consumers": {}
	},
	"my-post-consumer-addressbook-ui": {
		"name": "my-post-consumer-addressbook-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-consumer-track-ui": {
		"name": "my-post-consumer-track-ui",
		"version": "1.0.2",
		"consumers": {}
	},
	"my-post-dashboard-ui": {
		"name": "my-post-dashboard-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-e2e-integrations-tests": {
		"name": "my-post-e2e-integrations-tests",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-header-ui-DEPRECATED": {
		"name": "my-post-header-ui-DEPRECATED",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-home-postoffice-ui": {
		"name": "my-post-home-postoffice-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-marketing-ui": {
		"name": "my-post-marketing-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-redirect-ui": {
		"name": "my-post-redirect-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"my-post-shared-ui": {
		"name": "my-post-shared-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"mypost-consumer-signup": {
		"name": "mypost-consumer-signup",
		"version": "1.0.0",
		"consumers": {}
	},
	"parcel-collection-registration-ui": {
		"name": "parcel-collection-registration-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"po-box-lease-ui": {
		"name": "po-box-lease-ui",
		"version": "1.0.0",
		"consumers": {}
	},
	"mypost-business-marketing": {
		"name": "mypost-business-marketing",
		"version": "1.0.0",
		"consumers": {}
	},
	"mypost-business-mobile-mock": {
		"name": "mypost-business-mobile-mock",
		"version": "1.0.0",
		"consumers": {}
	},
	"mypost-business-shipping-ui": {
		"name": "mypost-business-shipping-ui",
		"version": "3.0.0-alpha",
		"consumers": {}
	},
	"mypost-business-ui": {
		"name": "mypost-business-ui",
		"version": "1.0.0-alpha",
		"consumers": {}
	},
	"mypostbusiness-api-mock": {
		"name": "mypostbusiness-api-mock",
		"version": "0.11.1",
		"consumers": {
			"mypost-business-ui": "0.1.5"
		}
	},
	"accessone-mock-server": {
		"name": "accessone-mock-server",
		"version": "1.0.3",
		"consumers": {
			"mypostbusiness-api-mock": "^0.9.1",
			"csso-api-mock": "^1.0.1"
		}
	},
	"accessone-ui-app-builder": {
		"name": "accessone-ui-app-builder",
		"version": "0.4.999",
		"consumers": {
			"accessone-ui-dev-server": "^0.4.999"
		}
	},
	"accessone-ui-dev-server": {
		"name": "accessone-ui-dev-server",
		"version": "0.4.999",
		"consumers": {}
	},
	"accessone-ui-project": {
		"name": "accessone-ui-project",
		"version": "1.9.2",
		"consumers": {
			"mypost-business-shipping-ui": "0.21.0",
			"mypost-business-ui": "^1.0.0",
			"accessone-ui-app-builder": "^0.4.999",
			"accessone-ui-dev-server": "^0.4.999",
			"accessone-ui-test-runner": "^0.4.999"
		}
	},
	"accessone-ui-test-runner": {
		"name": "accessone-ui-test-runner",
		"version": "0.4.999",
		"consumers": {}
	},
	"ap-test-module": {
		"name": "ap-test-module",
		"version": "0.0.10",
		"consumers": {}
	},
	"auspost-ab-modal": {
		"name": "auspost-ab-modal",
		"version": "1.0.0",
		"consumers": {
			"my-post-consumer-addressbook-ui": "^1.0.0",
			"my-post-consumer-track-ui": "^1.0.0"
		}
	},
	"auspost-address-completion": {
		"name": "auspost-address-completion",
		"version": "2.1.3",
		"consumers": {}
	},
	"auspost-analytics-adobe": {
		"name": "auspost-analytics-adobe",
		"version": "1.1.0",
		"consumers": {
			"auspost-identity-ui": "^1.1.0",
			"authority-to-leave-ui": "^1.0.3",
			"customer-account-management-ui": "^1.1.0",
			"mentor-ui": "^1.1.0",
			"my-post-consumer-addressbook-ui": "^1.1.0",
			"my-post-consumer-track-ui": "^1.1.0",
			"my-post-dashboard-ui": "^1.1.0",
			"my-post-home-postoffice-ui": "^1.1.0",
			"my-post-redirect-ui": "^1.0.4",
			"parcel-collection-registration-ui": "^1.0.3",
			"po-box-lease-ui": "^1.1.0",
			"auspost-ev": "0.0.10",
			"mypost-consumer-header": "^1.0.3"
		}
	},
	"auspost-angular-jasmine": {
		"name": "auspost-angular-jasmine",
		"version": "0.1.2",
		"consumers": {
			"mypost-business-shipping-ui": "^0.1.1"
		}
	},
	"auspost-angular-promiser": {
		"name": "auspost-angular-promiser",
		"version": "2.0.1",
		"consumers": {
			"mypost-business-shipping-ui": "^0.1.0"
		}
	},
	"auspost-angular-variable": {
		"name": "auspost-angular-variable",
		"version": "1.0.0",
		"consumers": {}
	},
	"auspost-array-includes": {
		"name": "auspost-array-includes",
		"version": "1.0.0",
		"consumers": {}
	},
	"auspost-browser-list": {
		"name": "auspost-browser-list",
		"version": "1.0.1",
		"consumers": {
			"auspost-gulp-es6": "^1.0.1",
			"auspost-styles": "^1.0.1",
			"auspost-ev-ui-demo": "^1.0.1",
			"my-post-redirect-ui": "^1.0.1",
			"customer-account-management-ui": "^1.0.1"
		}
	},
	"auspost-components": {
		"name": "auspost-components",
		"version": "1.0.3",
		"consumers": {}
	},
	"auspost-es6-module-seed": {
		"name": "auspost-es6-module-seed",
		"version": "1.0.0-alpha",
		"consumers": {}
	},
	"auspost-ev": {
		"name": "auspost-ev",
		"version": "1.3.7",
		"consumers": {
			"auspost-identity-ui": "^1.3.7"
		}
	},
	"auspost-feature-toggles": {
		"name": "auspost-feature-toggles",
		"version": "1.0.2",
		"consumers": {
			"my-post-authenticate-ui": "^1.0.2",
			"my-post-consumer-track-ui": "^1.0.2",
			"my-post-dashboard-ui": "^1.0.2"
		}
	},
	"auspost-message-exchange": {
		"name": "auspost-message-exchange",
		"version": "1.1.5",
		"consumers": {
			"my-post-consumer-addressbook-ui": "^1.1.2",
			"my-post-consumer-track-ui": "^1.1.2",
			"my-post-redirect-ui": "^1.1.1"
		}
	},
	"auspost-mobile-number-verification": {
		"name": "auspost-mobile-number-verification",
		"version": "1.2.7",
		"consumers": {
			"parcel-collection-registration-ui": "1.2.7"
		}
	},
	"auspost-mock-server": {
		"name": "auspost-mock-server",
		"version": "1.0.0-alpha",
		"consumers": {}
	},
	"csso-api-mock": {
		"name": "csso-api-mock",
		"version": "1.0.0",
		"consumers": {}
	},
	"eslint-config-auspost": {
		"name": "eslint-config-auspost",
		"version": "1.0.0",
		"consumers": {}
	},
	"mypost-consumer-address-completion": {
		"name": "mypost-consumer-address-completion",
		"version": "1.8.4",
		"consumers": {
			"auspost-identity-ui": "^1.8.3",
			"customer-account-management-ui": "^1.8.0",
			"mentor-ui": "^1.8.0",
			"my-post-consumer-addressbook-ui": "^1.8.0",
			"my-post-home-postoffice-ui": "^1.8.0",
			"my-post-redirect-ui": "^1.8.4"
		}
	},
	"mypost-consumer-auth": {
		"name": "mypost-consumer-auth",
		"version": "1.0.12",
		"consumers": {
			"auspost-identity-ui": "^1.0.2",
			"authority-to-leave-ui": "^1.0.5",
			"customer-account-management-ui": "^1.0.7",
			"mentor-ui": "^1.0.5",
			"my-post-consumer-addressbook-ui": "^1.0.5",
			"my-post-consumer-track-ui": "^1.0.7",
			"my-post-dashboard-ui": "^1.0.5",
			"my-post-home-postoffice-ui": "^1.0.5",
			"my-post-redirect-ui": "^1.0.7",
			"parcel-collection-registration-ui": "^1.0.5",
			"po-box-lease-ui": "^1.0.5",
			"auspost-ev": "^1.0.0",
			"mypost-consumer-header": "^1.0.8"
		}
	},
	"mypost-consumer-common": {
		"name": "mypost-consumer-common",
		"version": "2.1.24",
		"consumers": {
			"auspost-identity-ui": "^2.0.7",
			"authority-to-leave-ui": "^2.1.7",
			"customer-account-management-ui": "^2.1.7",
			"mentor-ui": "^2.1.7",
			"my-post-consumer-addressbook-ui": "^2.1.16",
			"my-post-consumer-track-ui": "^2.1.23",
			"my-post-dashboard-ui": "^2.1.13",
			"my-post-home-postoffice-ui": "^2.1.5",
			"my-post-redirect-ui": "^2.1.22",
			"parcel-collection-registration-ui": "^2.1.12",
			"po-box-lease-ui": "^2.1.18",
			"mypost-consumer-address-completion": "^2.1.3"
		}
	},
	"mypost-consumer-header": {
		"name": "mypost-consumer-header",
		"version": "4.4.2",
		"consumers": {
			"auspost-identity-ui": "^4.2.7",
			"authority-to-leave-ui": "^4.1.4",
			"customer-account-management-ui": "^4.3.8",
			"my-post-consumer-addressbook-ui": "^4.2.7",
			"my-post-consumer-track-ui": "^4.3.11",
			"my-post-dashboard-ui": "^4.2.7",
			"my-post-home-postoffice-ui": "^4.2.7",
			"my-post-redirect-ui": "^4.3.8",
			"parcel-collection-registration-ui": "^4.2.7",
			"po-box-lease-ui": "^4.2.7"
		}
	},
	"mypost-consumer-jshint-rules": {
		"name": "mypost-consumer-jshint-rules",
		"version": "1.0.3",
		"consumers": {
			"authority-to-leave-ui": "^1.0.3",
			"customer-account-management-ui": "^1.0.3",
			"mentor-ui": "^1.0.3"
		}
	},
	"mypost-consumer-styles": {
		"name": "mypost-consumer-styles",
		"version": "2.1.24",
		"consumers": {
			"auspost-identity-ui": "^2.1.12",
			"authority-to-leave-ui": "^2.1.6",
			"customer-account-management-ui": "^2.1.12",
			"mentor-ui": "^2.1.12",
			"my-post-authenticate-ui": "^2.1.10",
			"my-post-consumer-addressbook-ui": "^2.1.12",
			"my-post-consumer-track-ui": "^2.1.22",
			"my-post-dashboard-ui": "^2.1.12",
			"my-post-home-postoffice-ui": "^2.1.12",
			"my-post-redirect-ui": "^2.1.22",
			"my-post-shared-ui": "^1.0.0",
			"parcel-collection-registration-ui": "^2.1.12",
			"po-box-lease-ui": "^2.1.18",
			"auspost-ev": "^2.1.5",
			"auspost-mobile-number-verification": "^2.0.10",
			"mypost-consumer-address-completion": "^2.0.10"
		}
	},
	"securepay-mock": {
		"name": "securepay-mock",
		"version": "1.2.1",
		"consumers": {
			"mypost-business-shipping-ui": "^1.0.0"
		}
	}
};
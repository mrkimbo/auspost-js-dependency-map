'use strict';
/* global data */

var chart, packages;
var wheelData = {
  packageNames: [],
  matrix: []
};
var container = '#wheel';

function isDependency (pkg) {
  return data.some(function (item) {
    return pkg.name in (item.dependencies || {}) ||
      pkg.name in (item.devDependencies || {});
  });
}

function hasDependency (pkg) {
  function chk (val, key) {
    return data.some(function (item) {
      return item.name === key;
    });
  }

  return _.some(pkg.dependencies || {}, chk) || _.some(pkg.devDependencies || {}, chk);
}

function toggleOrphans (show) {
  if (show) {
    packages = data.slice();
  } else {
    packages = data.filter(function (pkg) {
      return isDependency(pkg) || hasDependency(pkg);
    });
  }

  wheelData.matrix = [];
  wheelData.packageNames = packages.map(function (item) {
    return item.name
  });
  render();
}

function createDependencyMatrix (item) {
  var matrix = new Array(packages.length).fill(0);

  _.forEach(item.dependencies, function (version, key) {
    var idx = wheelData.packageNames.indexOf(key);
    if (idx > -1) matrix[idx] = 1;
  });

  _.forEach(item.devDependencies, function (version, key) {
    var idx = wheelData.packageNames.indexOf(key);
    if (idx > -1) matrix[idx] = 1;
  });

  return matrix;
}

function destroy () {
  if (chart) {
    var el = document.querySelector(container);
    el.removeChild(el.firstElementChild);
    chart = undefined;
  }
}

// Build dependency matrix and render wheel chart
function render () {
  destroy();

  // re-create matrix
  packages.forEach(function (item, i) {
    wheelData.matrix[i] = createDependencyMatrix(item);
  });

  chart = d3.chart.dependencyWheel();
  d3.select(container)
    .datum(wheelData)
    .call(chart);

  console.log('Rendering ' + packages.length + ' of ' + data.length + ' items');
}

toggleOrphans(true);

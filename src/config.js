/* global __dirname */

import {getenv} from './lib';

export const STASH_USER = getenv('STASH_USER');
export const STASH_PASS = getenv('STASH_PASS');
export const STASH_PROTOCOL = getenv('STASH_PROTOCOL', 'https');
export const STASH_PORT = getenv('STASH_PORT', '');
export const STASH_HOST = getenv('STASH_HOST', 'stash.cd.auspost.com.au');
export const BASE_URL = `https://${STASH_USER}:${STASH_PASS}@${STASH_HOST}:${STASH_PORT}`;
export const DUMP_DIR = `${__dirname}/../app/data`;

// most likely projects to contain dependencies (comment out to scan all)
export const PROJECTS_TO_SCAN = [
    'NPM',
    'AUSPOST',
    'CSSO',
    'DCI',
    'EV',
    'LOC',
    'MRSO',
    'MP',
    'MB',
    'HELPSUPPORT',
    'AEMCMS'
];

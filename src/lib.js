import * as fs from 'fs';
import * as Config from './config';
import * as axios from 'axios';
import model from './model';

/** Pick up environment vars */
export function getenv(k, defV) {
    let val = process.env[k];
    if (typeof val !== 'string') {
        if (typeof defV === 'string') {
            return defV;
        } else {
            throw new Error(`Required environment variable: ${k} not found!`);
        }
    }
    return val;
}

/** construct repo's package url */
export const getPackageUrl = (projectKey, repoSlug) => {
    return `${Config.BASE_URL}/projects/${projectKey}/repos/${repoSlug}/browse/package.json?raw`;
    // return `${Config.BASE_URL}/projects/${projectKey}/repos/${repoSlug}/browse/package.json?raw&at=refs%2Fheads%2Fintegration`;
};

/** Extract and parse repo's package.json */
export async function getPackage(projectKey, repoSlug) {
    const url = getPackageUrl(projectKey, repoSlug);

    try {
        let res = await axios.get(url);
        return JSON.parse(res.data.lines.reduce((prev, curr) => {
            return prev + curr.text + '\n';
        }, ''));

    } catch (e) {
        console.warn(`- package.json not found:\n`, url.replace(/^.*?@/i, 'https://'), '\n');
    }
    return null;
};

/** save dependency results to model */
export const mapDependencies = (repo, deps = {}) => {
    Object.keys(deps).forEach((key) => {
        // console.log('mapDependencies', repo.name, key);
        if (model.consumerMap[key]) {
            model.consumerMap[key].consumers[repo.slug] = deps[key];
        }
    });
};

/** Export dependency map as js file for front-end to consume */
export const dump = (name, json) => {
    const filename = `${Config.DUMP_DIR}/${name}.js`;

    try {
        fs.accessSync(Config.DUMP_DIR, fs.F_OK);
    } catch (err) {
        fs.mkdirSync(Config.DUMP_DIR);
    }

    let content = `;var data = ${JSON.stringify(json, null, '\t')};`;
    fs.writeFile(filename, content, (err) => {
        console.error(err);
    });
};

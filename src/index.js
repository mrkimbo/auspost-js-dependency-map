import StashClient from './stash';
import * as Config from './config';
import * as lib from './lib';
import model from './model';

let stashClient = new StashClient({
  protocol: Config.STASH_PROTOCOL,
  hostname: Config.STASH_HOST,
  user: Config.STASH_USER,
  password: Config.STASH_PASS
});

/**
 * Collate repo names + versions
 * !! Uses default repo branch !!
 */
async function scanProjects () {
  let repo, project, info, repos;

  console.log('\n\nScan Project Info\n');

  for (project of model.projects) {
    let repos = await stashClient.repos(project.key);

    for (repo of repos) {
      console.log(`Reading project info: ${project.name}/${repo.name}`);
      console.log(`Reading project info: ${project.name}/${repo.name}`);

      info = await lib.getPackage(project.key, repo.slug);
      if (!info) continue;

      let { name, version, dependencies = {}, devDependencies = {} } = info;
      model.packages.push({
        name,
        version,
        dependencies,
        devDependencies,
        project,
        repo
      });
      model.consumerMap[repo.name] = {
        name: repo.name,
        version: info.version,
        consumers: {}
      };
    }
  }
}

/**
 * Scan repos for projects with NPM dependencies
 */
function scanDependencies () {
  let project, info;

  console.log('\n\nScan for dependencies\n');

  // scour other projects for NPM dependencies
  for (info of model.packages) {
    console.log(`checking dependencies in ${info.project.name}/${info.name}`);

    lib.mapDependencies(info.repo, info.dependencies);
    lib.mapDependencies(info.repo, info.devDependencies);
  }

  // save results:
  lib.dump('consumers', model.consumerMap);
  lib.dump('packages', model.packages.map(function (pkg) {
    delete pkg.project;
    delete pkg.repo;
    return pkg;
  }));
}

/**
 * Main process
 */
export async function main () {
  let allProjects = await stashClient.projects();
  model.projects = allProjects
    .filter((project) => Config.PROJECTS_TO_SCAN.indexOf(project.key) !== -1);

  await scanProjects();
  scanDependencies();
}

/** Run Main */
main().catch((e) => console.log(e));

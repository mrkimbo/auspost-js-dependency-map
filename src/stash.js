import { StashApi } from 'atlas-stash';


/** Adapt a stash api result from event emitter to promise */
function eeToPromise (apiResult) {
  return new Promise((resolve, reject) => {
    apiResult.on('error', (e) => reject(e));
    apiResult.on('allPages', (result) => resolve(result));
  });
}

/**
 * Adapts atlas-stash npm module api which is based on EventEmitter to a nicer,
 * promise-based API.
 */
export default class StashClient {
  stash;

  constructor (config) {
    this.stash = new StashApi(
      config.protocol,
      config.hostname,
      config.port,
      config.user,
      config.password
    );
  }

  /** Get list of visible stash projects. */
  projects () {
    return eeToPromise(this.stash.projects());
  }

  /** Get list of visible stash repos under a project. */
  repos (projectKey) {
    return eeToPromise(this.stash.repos(projectKey));
  }

  /** Get list of branches for a repo. */
  branches (projectKey, repoSlug) {
    return eeToPromise(this.stash.branches(projectKey, repoSlug));
  }

  /** Get list of commits for a branch. */
  commits (projectKey, repoSlug, branch) {
    return eeToPromise(this.stash.commits(projectKey, repoSlug, branch));

  }
}

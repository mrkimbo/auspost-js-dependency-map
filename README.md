# AusPost JS Dependency Map

## Summary
Quick hack project to spider the repositories inside Stash projects and map dependencies between them.

> Check out the [demo visualisation](http://tripleaxis.bitbucket.io/auspost/dependency-map/)

##### Installation / Setup
Requires Node >4.3.2

    npm install
    bower install
    
##### Data Generation
Running the project will generate two data files:
 - ```consumers.js``` an easy to read data-set of modules and what apps consume them
 - ```packages.js``` a dump of every module's package info, used by the dependency wheel visualisation

As the project is written in ES6, it needs to be transpiled with Babel before running.

    npm run build
    
Once this is done, execute the built index file to generate the data files.
The StashAPI requires your credentials in order to access project/repo information, so this needs to be added as 
Environment vars when running.

If using IntelliJ, create a new Node Run Configuration that executes the ```dist/index.js``` file and sets the following
environment vars: 

     STASH_HOST=stash.cd.auspost.com.au;STASH_USER=MyStashUsername;STASH_PASS=MyStashPassword

##### Data Display
Also included in the project is a dependency wheel visualisation.
Once the packages.js file has been generated, just open the ```app/index.html``` file in a static web server to view.
